#!/bin/bash

# Get the current user from the USER environment variable
CURRENT_USER=$USER

# Ask the user for sorting preference
echo "Sort processes by: (m)emory or (c)pu?"
read sort_option

echo "Processes running for user $CURRENT_USER:"

# Sort the processes based on user input
case $sort_option in
    m) ps aux --sort=-%mem | grep $CURRENT_USER ;;
    c) ps aux --sort=-%cpu | grep $CURRENT_USER ;;
    *) echo "Invalid option. Please choose 'm' for memory or 'c' for CPU." ;;
esac
