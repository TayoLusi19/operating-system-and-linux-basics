# Bash Script for Sorting User Processes

## Introduction
This Bash script is designed to display processes running for the current user, sorted by memory or CPU consumption based on user input. It's a simple yet powerful tool for system monitoring and management.

## Requirements
- A Linux or Unix-like operating system
- Bash shell

## Cloning the Repository
To run this script on your system, you first need to clone this repository. Here's how you can do it:

1. **Open your terminal.**
2. **Run the following command to clone the repository:**
`git clone https://gitlab.com/<your-gitlab-username>/user-processes-sorted.git`

Replace **<your-gitlab-username>** with your actual GitLab username where this repository is hosted.

## Installation
No specific installation is required. Just ensure you have Bash shell access and the ability to execute scripts.


## Usage
1. Open your terminal.
2. Navigate to the directory where the script is located: `cd user-processes-sorted`
3. Make the script executable (if it's not already): ``chmod +x user_processes_sorted.sh``

4. Run the script: `./user_processes_sorted.sh`
5. Follow the prompt:
- The script will ask: Sort processes by: (m)emory or (c)pu?
- Enter m for sorting by memory consumption.
- Enter c for sorting by CPU consumption.
- The script will then display the processes running for the current user, sorted according to your choice.

# Contributions
Contributions to this script are welcome. Please ensure to follow good coding practices and provide a detailed description of your changes or improvements.

# License
[LICENSE.md](https://gitlab.com/TayoLusi19/operating-system-and-linux-basics/-/blob/main/LICENSE.md)

# Author
Adetayo Michael Ibijemilusi

## Resume Bullet Point

- Authored user_processes_sorted.sh, a Bash script for dynamic sorting of user processes by CPU or memory usage, facilitating enhanced system monitoring.
- Engineered an interactive prompt within the script for user-driven sorting criteria, improving usability and customization for system administration tasks.
