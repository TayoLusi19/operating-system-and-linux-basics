#!/bin/bash

# Install the latest Java version
echo "Installing the latest Java version..."
sudo apt-get update
sudo apt-get install -y default-jdk

# Check if Java is installed
if ! type java > /dev/null 2>&1; then
    echo "Java is not installed."
    exit 1
fi

# Get Java version
JAVA_VERSION=$(java -version 2>&1 | awk -F '"' '/version/ {print $2}' | awk -F '.' '{print $1$2}')

# Convert Java version to an integer for comparison
JAVA_VERSION_NUM=$((10#$JAVA_VERSION))

# Check if the Java version is older than 11
if [[ JAVA_VERSION_NUM -lt 110 ]]; then
    echo "An older version of Java is installed: Java $JAVA_VERSION"
    exit 2
fi

# Check if Java version is 11 or higher
if [[ JAVA_VERSION_NUM -ge 110 ]]; then
    echo "Java version 11 or higher is installed: Java $JAVA_VERSION"
    exit 0
fi
