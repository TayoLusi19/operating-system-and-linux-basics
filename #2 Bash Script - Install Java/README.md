# Java Installation Script

This repository contains a script `install_java.sh` that automates the installation of the latest Java version on Debian-based systems. It checks if Java is installed, identifies the installed Java version, and verifies if the installed version is Java 11 or higher.

## Overview

The `install_java.sh` script performs several tasks:
1. Updates the package list.
2. Installs the latest default JDK package.
3. Checks if Java is installed on the system.
4. If installed, checks the Java version.
5. Prints messages based on the Java version installed, helping to identify if the installation is successful and meets the version requirements.

## Getting Started

To use this script, clone the repository and run the script in your Linux environment.

### Cloning the Repository

Run the following command to clone the repository:
 
- git clone https://gitlab.com/TayoLusi19/operating-system-and-linux-basics.git

### Running the Script

Navigate to the cloned directory and run the script:

- cd [Your Repository Name]
- chmod +x install_java.sh
- ./install_java.sh


## Script Details

Here's a brief overview of what the `install_java.sh` script does:

- **Update & Install:** Updates the system's package list and installs the latest Java Development Kit (JDK).
- **Java Version Check:** Checks if Java is installed and identifies the version.
- **Version Validation:** Validates whether the installed Java version is 11 or higher.

## Requirements

- Debian-based Linux distribution (e.g., Ubuntu)
- User with sudo privileges for installing packages

# Contributions
Contributions to this script are welcome. Please ensure to follow good coding practices and provide a detailed description of your changes or improvements.

# License
[LICENSE.md](https://gitlab.com/TayoLusi19/operating-system-and-linux-basics/-/blob/main/LICENSE.md)

# Author
Adetayo Michael Ibijemilusi

## Resume Bullet Point

- Designed install_java.sh, an automation script for seamless Java installation on Debian-based systems.
- Ensured compliance with Java version requirements, enhancing system readiness for development.

