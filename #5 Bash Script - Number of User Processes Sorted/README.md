# Exercise 5: Bash Script for Displaying User Processes Sorted

This exercise involves extending a bash script to include user interaction for specifying the number of processes to display, sorted by either memory or CPU usage. The script will prompt the user to input their sorting preference and the number of processes they wish to view.

## Prerequisites

- A Linux or Unix-like operating system
- Basic knowledge of terminal and command line usage
- Bash shell

## Script Overview

The script does the following:
1. Retrieves the current user's name using the `whoami` command.
2. Prompts the user to choose sorting by memory (m) or CPU (c).
3. Asks the user for the number of processes they would like to display.
4. Displays the top processes running for the current user, sorted according to the user's choice and limited to the specified number of processes.

## How to Run the Script

1. Open your terminal.
2. Create a new file named `processes.sh` and open it in your text editor.
3. Copy and paste the script provided into `processes.sh`.
4. Save the file and exit the text editor.
5. Make the script executable by running: `chmod +x processes.sh`.
6. Execute the script by typing `./processes.sh` and follow the on-screen prompts.

```bash
#!/bin/bash

# Get the current user from the USER environment variable
CURRENT_USER=$(whoami)

# Ask the user for sorting preference
echo "Sort processes by: (m)emory or (c)pu?"
read sort_option

# Ask the user for the number of processes to display
echo "How many processes would you like to display?"
read process_count

echo "Top $process_count processes running for user $CURRENT_USER sorted by $sort_option:"

# Filter, sort, and limit the number of processes based on user input
case $sort_option in
    m) ps aux | grep "^$CURRENT_USER" | sort -k4nr | head -n $process_count ;;
    c) ps aux | grep "^$CURRENT_USER" | sort -k3nr | head -n $process_count ;;
    *) echo "Invalid option. Please choose 'm' for memory or 'c' for CPU." ;;
esac
```

# Contributions
Contributions to this script are welcome. Please ensure to follow good coding practices and provide a detailed description of your changes or improvements.

# License
[LICENSE.md](https://gitlab.com/TayoLusi19/operating-system-and-linux-basics/-/blob/main/LICENSE.md)

# Author
Adetayo Michael Ibijemilusi

## Resume Bullet Point

- Enhanced processes.sh, adding interactive user input for process sorting by CPU or memory and specifying the display limit, improving user control and script functionality.
