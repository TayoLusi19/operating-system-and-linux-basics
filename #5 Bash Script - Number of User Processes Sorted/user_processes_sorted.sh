#!/bin/bash

# Get the current user from the USER environment variable
CURRENT_USER=$(whoami)

# Ask the user for sorting preference
echo "Sort processes by: (m)emory or (c)pu?"
read sort_option

# Ask the user for the number of processes to display
echo "How many processes would you like to display?"
read process_count

echo "Top $process_count processes running for user $CURRENT_USER sorted by $sort_option:"

# Filter, sort, and limit the number of processes based on user input
case $sort_option in
    m) ps aux | grep "^$CURRENT_USER" | sort -k4nr | head -n $process_count ;;
    c) ps aux | grep "^$CURRENT_USER" | sort -k3nr | head -n $process_count ;;
    *) echo "Invalid option. Please choose 'm' for memory or 'c' for CPU." ;;
esac
