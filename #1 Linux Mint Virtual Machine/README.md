# Linux Mint Virtual Machine Setup

This repository documents the process of setting up a Linux Mint Virtual Machine (VM). Linux Mint is a popular Linux distribution known for its user-friendliness and elegance. 

## Getting Started

These instructions will guide you through the process of setting up a Linux Mint VM on your computer.

### Prerequisites

- A computer with virtualization capabilities.
- Virtualization software (e.g., VirtualBox or VMware).
- Internet connection to download Linux Mint ISO.

### Installation

1. **Download Linux Mint ISO**:
   - Go to [Linux Mint website](https://linuxmint.com/download.php) and download the ISO file.

2. **Virtualization Software**:
   - Install a virtualization tool like VirtualBox or VMware.

3. **Create a New Virtual Machine**:
   - Open your virtualization software and set up a new VM.
   - Assign necessary resources (CPU, memory, disk space).
   - Use the downloaded Linux Mint ISO as the boot media.

4. **Install Linux Mint**:
   - Follow the on-screen instructions to install Linux Mint on the VM.
   - Restart the VM after installation, removing the installation media if prompted.

### Post-Installation Checks

After installing Linux Mint, you can perform the following checks:

1. **Distribution Details**:
   - `lsb_release -a` in the terminal will show Linux Mint version details.

2. **Package Manager**:
   - Linux Mint uses `apt` or `apt-get`. Test with `sudo apt update` or `sudo apt-get update`.

3. **CLI Editor**:
Check the default CLI editor with
1. vi --version
2. vim --version
3. nano --version

Installing Editor:

1. sudo apt vi install
2. sudo apt vim install
3. sudo apt nano install

4. **Software Manager**:
   - Linux Mint includes a GUI software manager, accessible from the menu.

5. **Configured Shell**:
   - Find out the configured shell with `echo $SHELL`.

# Contributions
Contributions to this script are welcome. Please ensure to follow good coding practices and provide a detailed description of your changes or improvements.

# License
[LICENSE.md](https://gitlab.com/TayoLusi19/operating-system-and-linux-basics/-/blob/main/LICENSE.md)

# Author
Adetayo Michael Ibijemilusi

## Resume Bullet Point

- Configured a Linux Mint VM using VirtualBox, optimizing resource allocation for improved performance.
- Managed Linux Mint setup, performing system updates and editor configurations to enhance development efficiency.

