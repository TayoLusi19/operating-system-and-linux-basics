# Operating System and Linux Basics



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/TayoLusi19/operating-system-and-linux-basics.git
git branch -M main
git push -uf origin main
```

# Resume Bullet Point


## Exercises 1
- Configured a Linux Mint VM using VirtualBox, optimizing resource allocation for improved performance.
- Managed Linux Mint setup, performing system updates and editor configurations to enhance development efficiency.

## Exercises 2
- Designed install_java.sh, an automation script for seamless Java installation on Debian-based systems.
- Ensured compliance with Java version requirements, enhancing system readiness for development.

## Exercises 3
- Developed script.sh for automating the tracking of user-specific processes in Linux, leveraging ps aux and grep for efficient process management.
- Optimized system monitoring by implementing a Bash script to identify and list all processes running under the user's account, enhancing operational transparency.

## Exercises 4
- Authored user_processes_sorted.sh, a Bash script for dynamic sorting of user processes by CPU or memory usage, facilitating enhanced system monitoring.
- Engineered an interactive prompt within the script for user-driven sorting criteria, improving usability and customization for system administration tasks.

## Exercises 5
- Enhanced processes.sh, adding interactive user input for process sorting by CPU or memory and specifying the display limit, improving user control and script functionality.

## Exercises 6
- Developed start-node-app.sh to automate Node.js application deployment, including installations, environment setup, and server initiation.
- Enhanced deployment efficiency by automating application setup and background server launch with environment-specific configurations.

## Exercises 7
- Expanded start-node-app.sh with checks for Node.js process activity and port availability, ensuring application operability.
- Integrated system verification for running state and network listening, validating successful application deployment.

## Exercises 8
- Enhanced start-node-app.sh to handle log directory setup, including creation and environment variable configuration, for Node.js application logging.
- Implemented script logic for log directory validation and LOG_DIR environment setting, facilitating organized log management.

## Exercises 9
- Modified start-node-app.sh to include service user myapp creation for running Node.js applications, enhancing security by limiting process permissions.
- Implemented application execution under myapp user, ensuring secure and restricted access to system resources during operation.
