# Exercise 9: Bash Script for Running Node.js Application with a Service User

This exercise extends the bash script to improve the security and management of a Node.js application by running it under a dedicated service user named `myapp`. This approach minimizes potential security risks by limiting the permissions and access of the application process.

## Script Features

The enhanced script:

- Checks if a specific service user (`myapp`) exists, and creates it if it does not.
- Sets up and runs the Node.js application under this service user, ensuring it operates with limited privileges.

## Prerequisites

- Completion of the setup and start steps from previous exercises.
- `sudo` privileges to create a user and install packages.
- The Node.js application and all related files must be accessible by the service user.

## How to Extend the Script

1. **Open your existing script file** (e.g., `start-node-app.sh`) in a text editor:

    ```bash
    nano start-node-app.sh
    ```

2. **Add the following code to manage the service user** before installing Node.js and NPM:

    ```bash
    #!/bin/bash

    # Check if a log directory argument is provided
    if [ $# -eq 0 ]; then
        echo "Usage: $0 log_directory"
        exit 1
    fi

    LOG_DIR=$1

    # Check if the log directory exists, create it if it does not
    if [ ! -d "$LOG_DIR" ]; then
        echo "Log directory does not exist, creating: $LOG_DIR"
        mkdir -p "$LOG_DIR"
    fi

    # Convert the log directory to an absolute path and export it
    LOG_DIR=$(realpath "$LOG_DIR")
    export LOG_DIR

    echo "Log directory set to: $LOG_DIR"

    # Create a service user 'myapp' if it does not exist
    if ! id "myapp" &>/dev/null; then
        echo "Creating service user 'myapp'"
        sudo useradd -r -s /bin/false myapp
    fi
    ```

3. **Ensure the application runs as the service user** by modifying the command to start the application:

    ```bash
    # Run the application as the service user 'myapp'
    sudo -u myapp node server.js &
    ```

4. **Save your changes** and exit the text editor.

## Running the Extended Script

- Execute the script with the log directory as a parameter:

    ```bash
    ./start-node-app.sh /path/to/log_directory
    ```

- The script will now include steps to ensure the application runs under the `myapp` service user, enhancing the security posture of your application deployment.

## Notes

- Running the application under a service user requires that all files and directories accessed by the application are readable (and writable, if necessary) by the `myapp` user.
- You might need to adjust file permissions or ownership accordingly, especially for the log directory and any files the application needs to access.

This extended script setup ensures your Node.js application runs more securely under a dedicated service user, following best practices for application deployment and management.

# Contributions
Contributions to this script are welcome. Please ensure to follow good coding practices and provide a detailed description of your changes or improvements.

# License
[LICENSE.md](https://gitlab.com/TayoLusi19/operating-system-and-linux-basics/-/blob/main/LICENSE.md)

# Author
Adetayo Michael Ibijemilusi

## Resume Bullet Point

- Modified start-node-app.sh to include service user myapp creation for running Node.js applications, enhancing security by limiting process permissions.
- Implemented application execution under myapp user, ensuring secure and restricted access to system resources during operation.
