#!/bin/bash

# Check if a log directory argument is provided
if [ $# -eq 0 ]; then
    echo "Usage: $0 log_directory"
    exit 1
fi

LOG_DIR=$1

# Check if the log directory exists, create it if it does not
if [ ! -d "$LOG_DIR" ]; then
    echo "Log directory does not exist, creating: $LOG_DIR"
    mkdir -p "$LOG_DIR"
fi

# Convert the log directory to an absolute path and export it
LOG_DIR=$(realpath "$LOG_DIR")
export LOG_DIR

echo "Log directory set to: $LOG_DIR"

# Create a service user 'myapp' if it does not exist
if ! id "myapp" &>/dev/null; then
    echo "Creating service user 'myapp'"
    sudo useradd -r -s /bin/false myapp
fi

# Install NodeJS and NPM (if not already installed)
sudo apt-get update
sudo apt-get install -y nodejs npm

# Print installed versions
echo "Node version: $(node -v)"
echo "NPM version: $(npm -v)"

# Download the artifact
wget https://node-envvars-artifact.s3.eu-west-2.amazonaws.com/bootcamp-node-envvars-project-1.0.0.tgz

# Unzip the downloaded file
tar -xzf bootcamp-node-envvars-project-1.0.0.tgz

# Change into the correct directory where your Node.js application is located
# Note: Replace 'path_to_node_app' with the actual path to your Node.js application directory
cd path_to_node_app

# Set environment variables (if needed)
export APP_ENV=dev
export DB_USER=myuser
export DB_PWD=mysecret

# Install dependencies
npm install

# Run the application as the service user 'myapp'
sudo -u myapp node server.js &

echo "Node app is running in the background, writing logs to $LOG_DIR"
