# User Processes Script

## Overview
This Bash script is designed to check and list all the processes running for the current user in a Linux environment. It utilizes the `ps aux` command combined with `grep` to filter the processes.

## Requirements
- A Linux-based operating system.
- Bash shell.
- Basic understanding of command-line operations.

## Installation
1. Download the `script.sh` file to your local machine.
2. Open your terminal and navigate to the directory containing `script.sh`.

## Usage
Before running the script, ensure that it is executable. If not, you can make it executable by running the following command in your terminal:


chmod +x script.sh

**To run the script, use the following command:**
./script.sh

The script will display a list of all the processes that are currently running under your user account.

## How It Works
The script uses the following command to list the processes:

ps aux | grep $USER

'ps aux' is a command to list all running processes.

'grep $USER' filters these processes to only include those that belong to the current user, where 'USER' is an environment variable representing the current user.

# Contributions
Contributions to this script are welcome. Please ensure to follow good coding practices and provide a detailed description of your changes or improvements.

# License
[LICENSE.md](https://gitlab.com/TayoLusi19/operating-system-and-linux-basics/-/blob/main/LICENSE.md)

# Author
Adetayo Michael Ibijemilusi

## Resume Bullet Point

- Developed script.sh for automating the tracking of user-specific processes in Linux, leveraging ps aux and grep for efficient process management.
- Optimized system monitoring by implementing a Bash script to identify and list all processes running under the user's account, enhancing operational transparency.
