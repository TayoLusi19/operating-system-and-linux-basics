#!/bin/bash
# Script to list processes of the current user

# Getting the current user
CURRENT_USER=$USER

# Listing processes for the current user
ps aux | grep $CURRENT_USER
