# Exercise 8: Bash Script for Node.js Application with Log Directory

This exercise expands on the previous bash script by adding functionality to manage a log directory for the Node.js application. The script will now accept a parameter for the log directory where the application will write its logs. It ensures that the directory exists, creates it if it doesn't, and sets an environment variable `LOG_DIR` to its absolute path before running the application.

## Script Features

The extended script:

- Accepts a log directory as a command-line argument.
- Checks if the provided log directory exists, creates it if it doesn't.
- Sets the `LOG_DIR` environment variable to the absolute path of the log directory.
- Proceeds with the steps to set up and run the Node.js application, ensuring it writes logs to the specified directory.

## Prerequisites

- Completion of the setup and start steps from previous exercises.
- `realpath` command available for converting paths to absolute paths.

## How to Extend the Script

1. **Open your existing script file** (e.g., `start-node-app.sh`) in a text editor:

    ```bash
    nano start-node-app.sh
    ```

2. **Add the following code at the beginning of your script**:

    ```bash
    #!/bin/bash

    # Check if a log directory argument is provided
    if [ $# -eq 0 ]; then
        echo "Usage: $0 log_directory"
        exit 1
    fi

    LOG_DIR=$1

    # Check if the log directory exists, create it if it does not
    if [ ! -d "$LOG_DIR" ]; then
        echo "Log directory does not exist, creating: $LOG_DIR"
        mkdir -p "$LOG_DIR"
    fi

    # Convert the log directory to an absolute path and export it
    LOG_DIR=$(realpath "$LOG_DIR")
    export LOG_DIR

    echo "Log directory set to: $LOG_DIR"
    ```

3. **Integrate the remaining setup and startup steps** from previous exercises below this new block.

4. **Save your changes** and exit the text editor.

## Running the Extended Script

- Run the script with the log directory as a parameter:

    ```bash
    ./start-node-app.sh /path/to/log_directory
    ```

- The script will check and prepare the log directory, set the `LOG_DIR` environment variable, and start the Node.js application. Logs will be written to the specified directory.

## Notes

- Ensure your Node.js application is configured to use the `LOG_DIR` environment variable for its logging output.
- If the application does not automatically create or manage log files within the `LOG_DIR`, you may need to implement or configure logging within your application code.

By following these steps, you will enhance the deployment script to include log directory management, ensuring your Node.js application can log its output effectively.

# Contributions
Contributions to this script are welcome. Please ensure to follow good coding practices and provide a detailed description of your changes or improvements.

# License
[LICENSE.md](https://gitlab.com/TayoLusi19/operating-system-and-linux-basics/-/blob/main/LICENSE.md)

# Author
Adetayo Michael Ibijemilusi

## Resume Bullet Point

- Enhanced start-node-app.sh to handle log directory setup, including creation and environment variable configuration, for Node.js application logging.
- Implemented script logic for log directory validation and LOG_DIR environment setting, facilitating organized log management.
