# Exercise 6: Bash Script to Start a Node.js Application

This exercise guides you through writing a bash script to automate the setup and launch of a Node.js application on a server. The script will install Node.js and npm, download the application artifact, set required environment variables, and start the application in the background.

## Prerequisites

- A Unix/Linux environment
- `sudo` privileges for installing Node.js and npm
- `curl` or `wget` installed for downloading files
- `tar` command available for unzipping files

## Script Breakdown

The script performs the following operations:

1. **Updates the package list** and **installs Node.js and npm** on your system.
2. **Prints the installed versions** of Node.js and npm to confirm successful installation.
3. **Downloads the Node.js application artifact** from a specified URL using `wget`.
4. **Unzips the downloaded file** to extract the application.
5. **Sets environment variables** required for the application to run correctly.
6. **Changes into the application directory**, installs dependencies, and **starts the server** in the background.

## How to Use the Script

1. **Open your terminal.**

2. **Create a new bash script file** named `start-node-app.sh`:

    ```bash
    touch start-node-app.sh && nano start-node-app.sh
    ```
    Replace `nano` with your preferred text editor if desired.

3. **Copy and paste the following script into `start-node-app.sh`**:

    ```bash
    #!/bin/bash

    # Install NodeJS and NPM
    sudo apt-get update
    sudo apt-get install -y nodejs npm

    # Print installed versions
    echo "Node version: $(node -v)"
    echo "NPM version: $(npm -v)"

    # Download the artifact
    wget https://node-envvars-artifact.s3.eu-west-2.amazonaws.com/bootcamp-node-envvars-project-1.0.0.tgz

    # Unzip the downloaded file
    tar -xzf bootcamp-node-envvars-project-1.0.0.tgz

    # Change into the correct directory
    cd package

    # Set environment variables
    export APP_ENV=dev
    export DB_USER=myuser
    export DB_PWD=mysecret

    # Install dependencies and run the application
    npm install
    node server.js &

    echo "Node app is running in the background"
    ```

4. **Save the file and exit your text editor.**

5. **Make the script executable:**

    ```bash
    chmod +x start-node-app.sh
    ```

6. **Run the script:**

    ```bash
    ./start-node-app.sh
    ```

    Follow any on-screen prompts. The script will execute all the steps mentioned above, and the Node.js application will start running in the background.

## Notes

- The script assumes you are using a Debian-based system (like Ubuntu) for the `apt-get` command.
- Ensure the Node.js application is configured to exit with an error message if the required environment variables are not set.
- The warning about the `LOG_DIR` variable not being set is known and can be ignored as per the exercise instructions.

By following these steps, you should be able to automate the deployment of a Node.js application with the necessary environment configurations.


# Contributions
Contributions to this script are welcome. Please ensure to follow good coding practices and provide a detailed description of your changes or improvements.

# License
[LICENSE.md](https://gitlab.com/TayoLusi19/operating-system-and-linux-basics/-/blob/main/LICENSE.md)

# Author
Adetayo Michael Ibijemilusi

## Resume Bullet Point

- Developed start-node-app.sh to automate Node.js application deployment, including installations, environment setup, and server initiation.
- Enhanced deployment efficiency by automating application setup and background server launch with environment-specific configurations.
