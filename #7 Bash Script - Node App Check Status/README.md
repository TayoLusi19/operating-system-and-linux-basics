# Exercise 7: Bash Script for Checking Node.js Application Status

This exercise builds upon previous exercises by adding functionality to the bash script to check whether the Node.js application has successfully started. It verifies the running process of the application and checks the port it listens on.

## Script Features

The extended script performs the following additional checks:

- **Verifies that the Node.js application process is running** by searching for the process name or command used to start it.
- **Checks if the specified port is in use**, indicating the application is listening on that port.

## Prerequisites

- Completion of the setup and start steps from previous exercises.
- `netstat` command available for checking port usage (part of the `net-tools` package on most Linux distributions).
- Node.js application configured to start on a known port.

## How to Extend the Script

1. **Open your existing script file** (e.g., `start-node-app.sh`) in a text editor:

    ```bash
    nano start-node-app.sh
    ```

2. **Add the following code to the end of your script**:

    ```bash
    #!/bin/bash

    # Name or part of the command of the Node.js script to identify the process
    NODE_APP_NAME="server.js"

    # Specify the port the app is expected to listen on
    APP_PORT=3000

    # Function to check if a port is in use
    is_port_in_use() {
        netstat -tuln | grep ":$1" > /dev/null
    }

    # Start the Node.js application (assuming previous steps are included here)

    # Wait for a moment to let the application start
    sleep 5

    # Check if the Node.js process is running
    PID=$(pgrep -f "$NODE_APP_NAME")
    if [ -z "$PID" ]; then
        echo "Node.js application is not running."
        exit 1
    else
        echo "Node.js application is running. PID: $PID"
    fi

    # Check if the application is listening on the specified port
    if is_port_in_use $APP_PORT; then
        echo "Node.js application is listening on port $APP_PORT."
    else
        echo "Node.js application is not listening on port $APP_PORT."
        exit 1
    fi
    ```

3. **Save your changes** and exit the text editor.

## Running the Extended Script

- Ensure the Node.js application and all required services are correctly configured and can start without errors.
- Run the script as before:
  
    ```bash
    ./start-node-app.sh
    ```

- The script will now include checks for the application's process and listening port, providing feedback on the application's status.

## Notes

- This script assumes the application is expected to listen on a specific port (`3000` in this example). Adjust the `APP_PORT` variable as necessary to match your application's configuration.
- Ensure `net-tools` is installed on your system to use the `netstat` command. If not, you can install it using your package manager (e.g., `sudo apt-get install net-tools` on Debian/Ubuntu).

By following these steps, you extend the functionality of your deployment script to include vital checks that ensure your Node.js application is running correctly.

# Contributions
Contributions to this script are welcome. Please ensure to follow good coding practices and provide a detailed description of your changes or improvements.

# License
[LICENSE.md](https://gitlab.com/TayoLusi19/operating-system-and-linux-basics/-/blob/main/LICENSE.md)

# Author
Adetayo Michael Ibijemilusi

## Resume Bullet Point

- Expanded start-node-app.sh with checks for Node.js process activity and port availability, ensuring application operability.
- Integrated system verification for running state and network listening, validating successful application deployment.
