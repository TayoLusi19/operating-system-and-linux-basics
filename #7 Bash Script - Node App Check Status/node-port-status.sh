#!/bin/bash

# Name or part of the command of the Node.js script to identify the process
NODE_APP_NAME="server.js"

# Specify the port the app is expected to listen on
APP_PORT=3000

# Function to check if a port is in use
is_port_in_use() {
    netstat -tuln | grep ":$1" > /dev/null
}

# Start the Node.js application (assuming previous steps are included here)

# Wait for a moment to let the application start
sleep 5

# Check if the Node.js process is running
PID=$(pgrep -f "$NODE_APP_NAME")
if [ -z "$PID" ]; then
    echo "Node.js application is not running."
    exit 1
else
    echo "Node.js application is running. PID: $PID"
fi

# Check if the application is listening on the specified port
if is_port_in_use $APP_PORT; then
    echo "Node.js application is listening on port $APP_PORT."
else
    echo "Node.js application is not listening on port $APP_PORT."
    exit 1
fi
